# Fortify GitLab CI Templates

Build secure software fast with [Fortify](https://www.microfocus.com/en-us/solutions/application-security). Fortify offers end-to-end application security solutions with the flexibility of testing on-premises and on-demand to scale and cover the entire software development lifecycle.  With Fortify, find security issues early and fix at the speed of DevOps. 

The Fortify GitLab CI Templates repository contains a collection of individual GitLab job .yml templates for integrating Fortify application security solutions into your GitLab CI/CD pipeline. These templates can be included directly into existing .gitlab-ci.yml definitions and are designed to be extensible through overridable variables. For more advanced use cases and customization, simply use the templates as a reference to get started.  

If you are not already a Fortify customer, check out our [Free Trial](https://www.microfocus.com/en-us/products/application-security-testing/free-trial).

# General Usage
The easist way to get started is to directly include one or more reference templates into an existing .gitlab-ci.yml file. Each template uses variables to parameterize functionality and provide flexible configuration through use of overrides. When integrating any of the templates, here are some common considerations to keep in mind:
* Be sure to consider the appropriate job triggering rules when integrating application security testing, based on your project, branching strategy and release cadence. For example, users may choose to only perform comprehensive SAST analysis when merging or commiting changes to master or on a pre-defined schedule (daily, weekly, etc). DAST jobs should only be run after code is deployed as another example.
* Unless variables are explicitly defined in the reference template, the variables should exist in the GitLab Settings -> CI/CD configuration. Masked variables should be used wherever possible. If you use protected variables, be sure the branches where you want to execute the Fortify jobs are also marked as protected to avoid job failure.
* Review the default values in the templates and override to configure as appropriate for your project/environment.
* Consider using GitLab's cache functionality to optimize pipeline execution time, particularly when using the ScanCentral functionality for packaging code with Maven or Gradle builds.
* Check out the [Eightball](https://gitlab.com/Fortify/example-eightball) project to see example template implementations using remote includes and overridding of default variable values.

# Available Templates
## Fortify on Demand SAST (fortify-sast-fod.yml)
Perform a comprehensive Static Application Security Testing (SAST) assessment using Fortify on Demand (FoD). The fortify-sast-fod.yml template uses the Fortify ScanCentral client to prepare a zip file of the project source code and dependencies, and then invokes the FoDUploader utility to start a SAST scan in Fortify on Demand using the prepared payload.

The invocation of both the ScanCentral client and FoDUploader can be modified to fit each individual project and CI/CD pipeline needs.  Please refer to the following resources for more information on customizing these two steps:
* [FoDUploader documentation](https://github.com/fod-dev/fod-uploader-java)
* [ScanCentral documentation](https://www.microfocus.com/documentation/fortify-software-security-center/)  

Note that in combination with FoD Uploader, only the ScanCentral Package command is relevant. Other ScanCentral commands are not used in combination with FoD Uploader, and none of the other ScanCentral components (ScanCentral Controller, Sensor, etc) are used when submitting scans to Fortify on Demand. 

Preparing source and dependencies for projects developed using .NET, C/C++, and Swift/Objective-C require additional packaging and/or compilation steps beyond the scope of this reference template. Please review the Fortify on Demand Documentation for packaging instructions for these technologies. The Fortify on Demand scan can be integrated into your CI/CD pipeline by downloading the latest FoDUploader release onto your runner as part of the pipeline.

Required GitLab CI/CD variables:
* $FOD_USERNAME
* $FOD_PAT
* $FOD_TENANT
* $FOD_RELEASE

For projects with a mature DevSecOps process, you may choose to override the FOD_UPLOADER_OPTS environment variable to add 'break the build' functionality. FoD allows security leads to define flexible Security Policies, and each release is assigned a Passing or Failing status based on compliance with the Security Policy. In this use case, FoDUploader will wait for the FoD SAST scan to complete and then optionally cause the job and/or pipeline to fail.

## Fortify ScanCentral SAST (fortify-sast-scancentral.yml)
Perform a comprehensive Static Application Security Testing (SAST) assessment using your on-premises Fortify ScanCentral environment. The fortify-sast-scancentral.yml template uses the Fortify ScanCentral client to prepare a zip file of the project source code and dependencies and then  start a SAST scan in Fortify Software Security Center/ScanCentral using the prepared payload. 

The invocation of the ScanCentral client should be modified to fit each individual project and CI/CD pipeline needs.  Please refer to the following resources for more information on customizing the template:
* [ScanCentral documentation](https://www.microfocus.com/documentation/fortify-software-security-center/)  

The template uses a Linux image and does not supoprt preparation of .NET, C/C++, and Swift/Objective-C technologies using ScanCentral. Fortify on-premises customers should utilize Fortify Static Code Analyzer (SCA) to integrate local translation and/or scanning on the GitLab runner.

Required GitLab CI/CD variables:
* $SSC_URL
* $SSC_TOKEN

## Fortify on Demand DAST (fortify-dast-fod.yml)
Under construction

## Fortify ScanCentarl DAST (fortify-dast-scancentral.yml)
Under construction

